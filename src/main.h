/* header */
//////////////////
/// PROTOTYPES ///
//////////////////
void vigie_Wifi(void);
void wifi_AP(void);
void start_wifiAP(void);
void rtc_init(void);
void scribe_sd (void);
void sd_init(void);
void compte_tour(void);
void stop_compteur(void);
void start_compteur(void);
void stop_moteur(void);
void gaz_moteur(void);
void start_moteur(void);
