#include <Arduino.h>
#include <ESP32Servo.h>
#include "FS.h"
#include "SD.h"
#include "SPI.h"
#include "RTClib.h"
#include "Wire.h"
#include "driver/pcnt.h"
#include <WiFi.h>
#include <ESPAsyncWebServer.h>
#include "main.h"
//#include <time.h>

/////////////////////////////
/* DÉFINITIONS DU COMPTEUR */
/////////////////////////////
#define PCNT_TEST_UNIT PCNT_UNIT_0 // Il existe 8 compteurs de 0 à 7
#define PCNT_TEST_CHANNEL PCNT_CHANNEL_0
#define PCNT_INPUT_SIG_IO   GPIO_NUM_15 // Pulse Input GPIO
#define PCNT_INPUT_CTRL_IO  GPIO_NUM_16  // Control GPIO HIGH=count up, LOW=count down
#define PCNT_H_LIM_VAL      10000 //valeur max du depart_compteur_minuteur
#define PCNT_L_LIM_VAL      -10 //valeur mini du compteur
bool sem_scribe = false; // sémaphore pour le compteur
int16_t count = 0; // variable de compteur
int16_t old_count = 0;
int vRotReel; // vitesse réelle mesurée du moteur
unsigned long chrono_scribe = 0;
unsigned long start_chrono_scribe = 0; // minuteur pour le déclenchement du compteur
bool sema_compte_tour = false; // semaphore poour le compte tour
unsigned long depart_compte_tour = 0; // valeur de millis au déclenchement du compte-tour
int intervalle_compte_tour = 5000; // durée entre deux mesures du compte-tour en ms
int pptr = 2; //nombre de pulsations par tour d'hélice
//unsigned long chrono_compte_tour;

/////////////////////////////////
/*NUMÉRO DE SÉRIE DE L'APPAREIL*/
/////////////////////////////////
char numero_capteur[] = "0002";

///////////////////////////
/* DÉFINITIONS DE LA RTC */
///////////////////////////
RTC_DS3231 rtc; //déclaration de la rtc
DateTime now{rtc.now()};

///////////////////////////
/* DÉFINITIONS DE LA LED */
///////////////////////////
#define LED 13

////////////////////////////////
/* DÉFINITIONS DE LA CARTE SD */
////////////////////////////////
unsigned long inter_ecriture = 60000; // durée entre deux écritures sur la carte SD en millisecondes
char horodatage[16]; //création du tableau pour contenir l'horodatage
char fichier[16]; //tableau pour le nom de fichier

//////////////////////////////////
/* DÉFINITIONS DU DRIVER MOTEUR */
//////////////////////////////////
int vRotVis = 2400; //vitesse de rotation recherchée en tr/min
Servo moteur; //création de l'objet moteur
int minUs = 1000; // intervalle pwm mini
int maxUs = 2000; // intervalle pwm maxi
int pos = 142; // vitesse de départ, 135 donne environ 40Hz
int ecart_gaz = 10;
int moteurPin = 4; //pin de contrôle du BEC de l'ESC du moteur
ESP32PWM pwm; // Activation du pwm

////////////////////////////////
/* DÉFINITIONS DU BOUTON WIFI */
////////////////////////////////
int bouton_wifi = 2; // pin du bouton wifi
bool sem_wifi = false; //sémaphore du minuteur du bouton wifi
unsigned long wifi_minuteur = 0; // minuteur pour l'appui long sur le bouton wifi
bool etat_bouton_wifi;
int tempo_bouton_wifi = 5000; //temps d'appui en millisecondes pour arrêter le capteur et passer en mode wifi 

///////////////////////////////////////
/* DÉFINITIONS DU POINT D'ACCES WIFI */
///////////////////////////////////////
const char *ssid = "IFV-Sporix"; // SSID
const char *password = "12345678"; // PASSWORD, 8 caractères minimum
WiFiServer server(80); // port du serveur



////////////////////////////
/// FONCTION CODE ERREUR ///
////////////////////////////

void errorCode(int codeNumber) {
  while(1) {
    delay(2700);
    for( int i=1 ; i<=codeNumber ; ++i ){
      Serial.println("balise errCode Blink");
      delay(300);
      digitalWrite(LED,HIGH);
      delay(300);
      digitalWrite(LED,LOW);
      delay(300); 
    }
  }
}

/////////////////////////
/* FONCTIONS DU MOTEUR */
/////////////////////////

void start_moteur() {
  ESP32PWM::allocateTimer(2);
  moteur.setPeriodHertz(50);
  moteur.attach(moteurPin, minUs, maxUs);
  delay(15);
  moteur.write(0); // envoi du 0 à l'ESC pour initialisation
  delay(1500); // délai permettant au variateur de détecter le zero
  moteur.write(pos); // vitesse initiale
}

void gaz_moteur() {
  if (vRotVis > vRotReel - ecart_gaz  && pos < 170) {
    pos ++;
  }
  if (vRotReel > vRotVis + ecart_gaz && pos > 0) {
    pos --;
  }
  moteur.write(pos);
  Serial.print("count =");
  Serial.println(count);
  Serial.print("Tr/min = ");
  Serial.println(vRotReel);
  Serial.print("gaz = ");
  Serial.println(pos);

}

void stop_moteur() {
  moteur.write(0);
  delay(1000);
  moteur.detach();
}



///////////////////////////
/* FONCTIONS DU COMPTEUR */
///////////////////////////

void start_compteur() {

  esp_err_t error; 
  Serial.println("Initialisation du compteur");

  pcnt_config_t pcnt_config = {
    PCNT_INPUT_SIG_IO, // Pulse input gpio_num, if you want to use gpio16, pulse_gpio_num = 16, a negative value will be ignored
    PCNT_PIN_NOT_USED, // Control signal input gpio_num, a negative value will be ignored
    PCNT_MODE_KEEP, // PCNT low control mode
    PCNT_MODE_KEEP, // PCNT high control mode
    PCNT_COUNT_INC, // PCNT positive edge count mode
    PCNT_COUNT_DIS, // PCNT negative edge count mode
    //PCNT_H_LIM_VAL, // Maximum counter value
    //PCNT_L_LIM_VAL, // Minimum counter value
    PCNT_TEST_UNIT, // PCNT unit number
    PCNT_TEST_CHANNEL, // the PCNT channel
  };

  if(pcnt_unit_config(&pcnt_config) == ESP_OK) //init unit
    Serial.println("Config Unit_0 = ESP_OK");
  pcnt_set_filter_value(PCNT_TEST_UNIT, 100); /*Configure input filter value*/
  pcnt_filter_enable(PCNT_TEST_UNIT); /*Enable input filter*/
  pcnt_counter_pause(PCNT_TEST_UNIT);
  pcnt_counter_clear(PCNT_TEST_UNIT); /*Reset counter value*/
  pcnt_counter_resume(PCNT_TEST_UNIT); /*Resume counting*/
}

void stop_compteur() {
  /*Pause counter*/
  pcnt_counter_pause(PCNT_TEST_UNIT);
  /*Reset counter value*/
  pcnt_counter_clear(PCNT_TEST_UNIT);
  // peut-être libérer la mémoire compteur?
}

void compte_tour() {
  if (sema_compte_tour == false){
    sema_compte_tour = true;
    depart_compte_tour = millis();
  }
  int offset = (millis() - depart_compte_tour );
  
  if (offset >= intervalle_compte_tour && sema_compte_tour == true){
    //chrono_compte_tour = millis()- start_chrono_scribe;
    pcnt_get_counter_value(PCNT_UNIT_0, &count);
    int diff_count = count - old_count;
    vRotReel = (60000*diff_count/(pptr*offset)); //vitesse en tours par min (4 pulsations /tour, 2 montantes, 2 descendantes)
    old_count = count;
    //Serial.println("count =");
    //Serial.println(count);
    //Serial.println("chrono_compte_tour=");
    //Serial.println(chrono_compte_tour);
    //Serial.println("vRrotReel=");
    //Serial.println(vRotReel);
    gaz_moteur();// ajustement des gaz en fonction de la vitesse mesurée
    sema_compte_tour = false;
  } 
  
}


/////////////////////////
/// FONCTIONS SD CARD ///
/////////////////////////

void listDir(fs::FS &fs, const char * dirname, uint8_t levels){
    Serial.printf("Listing directory: %s\n", dirname);

    File root = fs.open(dirname);
    if(!root){
        Serial.println("Failed to open directory");
        return;
    }
    if(!root.isDirectory()){
        Serial.println("Not a directory");
        return;
    }

    File file = root.openNextFile();
    while(file){
        if(file.isDirectory()){
            Serial.print("  DIR : ");
            Serial.println(file.name());
            if(levels){
                listDir(fs, file.name(), levels -1);
            }
        } else {
            Serial.print("  FILE: ");
            Serial.print(file.name());
            Serial.print("  SIZE: ");
            Serial.println(file.size());
        }
        file = root.openNextFile();
    }
}

void createDir(fs::FS &fs, const char * path){
    Serial.printf("Creating Dir: %s\n", path);
    if(fs.mkdir(path)){
        Serial.println("Dir created");
    } else {
        Serial.println("mkdir failed");
    }
}

void removeDir(fs::FS &fs, const char * path){
    Serial.printf("Removing Dir: %s\n", path);
    if(fs.rmdir(path)){
        Serial.println("Dir removed");
    } 
    else {
        Serial.println("rmdir failed");
    }
}

void readFile(fs::FS &fs, const char * path){
    Serial.printf("Reading file: %s\n", path);

    File file = fs.open(path);
    if(!file){
        Serial.println("Failed to open file for reading");
        return;
    }

    Serial.print("Read from file: ");
    while(file.available()){
        Serial.write(file.read());
    }
    file.close();
}

void writeFile(fs::FS &fs, const char * path, const char * message){
    Serial.printf("Writing file: %s\n", path);

    File file = fs.open(path, FILE_WRITE);
    if(!file){
        Serial.println("Failed to open file for writing");
        errorCode(2);
        return;
    }
    if(file.print(message)){
        Serial.println("File written");
    } else {
        Serial.println("Write failed");
    }
    file.close();
}

void appendFile(fs::FS &fs, const char * path, const char * message){
    Serial.printf("Appending to file: %s\n", path);

    File file = fs.open(path, FILE_APPEND);
    if(!file){
        Serial.println("Failed to open file for appending");
        return;
    }
    if(file.print(message)){
        Serial.println("Message appended");
    } else {
        Serial.println("Append failed"); 
    }
    file.close();
}

void renameFile(fs::FS &fs, const char * path1, const char * path2){
    Serial.printf("Renaming file %s to %s\n", path1, path2);
    if (fs.rename(path1, path2)) {
        Serial.println("File renamed");
    } else {
        Serial.println("Rename failed");
    }
}

void deleteFile(fs::FS &fs, const char * path){
    Serial.printf("Deleting file: %s\n", path);
    if(fs.remove(path)){
        Serial.println("File deleted");
    } else {
        Serial.println("Delete failed");
    }
}

void testFileIO(fs::FS &fs, const char * path){
    File file = fs.open(path);
    static uint8_t buf[512];
    size_t len = 0;
    uint32_t start = millis();
    uint32_t end = start;
    if(file){
        len = file.size();
        size_t flen = len;
        start = millis();
        while(len){
            size_t toRead = len;
            if(toRead > 512){
                toRead = 512;
            }
            file.read(buf, toRead);
            len -= toRead;
        }
        end = millis() - start;
        Serial.printf("%u bytes read for %u ms\n", flen, end);
        file.close();
    } else {
        Serial.println("Failed to open file for reading");
    }


    file = fs.open(path, FILE_WRITE);
    if(!file){
        Serial.println("Failed to open file for writing");
        return;
    }

    size_t i;
    start = millis();
    for(i=0; i<2048; i++){
        file.write(buf, 512);
    }
    end = millis() - start;
    Serial.printf("%u bytes written for %u ms\n", 2048 * 512, end);
    file.close();
}

void sd_init() {

  uint8_t cardType = SD.cardType();

  if(cardType == CARD_NONE){
      Serial.println("No SD card attached");
      errorCode(4);
      return;
  }
  
  if(!SD.begin()){
      Serial.println("Card Mount Failed");
      errorCode(3);
      return;
  }

  
   
  
  uint64_t cardSize = SD.cardSize() / (1024 * 1024);
    Serial.printf("SD Card Size: %lluMB\n", cardSize);

  now = rtc.now();
  char date_format[] = "DDhhmm";
  char *date = now.toString(date_format);  
  strcat(fichier,date);
  char entete[64] = "# YY-MM-DD:hh:mm:ss Vrot en tr/min \n";
  writeFile(SD, fichier, "capteur n° ");
  appendFile(SD, fichier, numero_capteur );
  appendFile(SD, fichier, "\n");
  appendFile(SD, fichier, entete);
}

void scribe_sd (){
  if ( sem_scribe == false ) {
    sem_scribe = true;
    pcnt_counter_pause(PCNT_TEST_UNIT);
    pcnt_counter_clear(PCNT_TEST_UNIT);
    pcnt_counter_resume(PCNT_TEST_UNIT);
    start_chrono_scribe = millis();
  }
  chrono_scribe = millis() - start_chrono_scribe;
  //Serial.println("sem_scribe=");
  //Serial.println(sem_scribe);

  if ( sem_scribe == true && chrono_scribe >= inter_ecriture ){
    //Serial.println("balise scribe");
    int vRotMoyen = count*60000/(pptr*chrono_scribe);
    DateTime time{rtc.now()};
    char timestamp[] = "YY-MM-DD-hh:mm:ss";
    char *horodatage = time.toString(timestamp);
    char buffer[64];
    snprintf(buffer, sizeof buffer, "%d", vRotMoyen);
    appendFile(SD, fichier, horodatage);
    appendFile(SD, fichier, " "  );
    appendFile(SD, fichier, buffer);
    appendFile(SD, fichier, "\n"  );
    Serial.println("ECRITURE SUR SD");
    sem_scribe = false;
  }
}


/////////////////////
/// FONCTIONS RTC ///
/////////////////////

void rtc_init() {
  if (! rtc.begin()) {
      Serial.println("RTC introuvable !"); // ligne de debug à commenter en prod
      delay(2000);
      errorCode(1);
      ESP.restart();
     //while (1);
    }
     if (rtc.lostPower()) {
       Serial.println("Veuillez régler l'heure et vérifier la pile du module RTC!"); // ligne de debug à commenter en prod
       errorCode(5);
     }
    else
    {
      //DateTime now = rtc.now(); //
      Serial.println("rtc OK");
    }
    
  
}

//////////////////////
/// FONCTIONS WIFI ///
//////////////////////

void start_wifiAP() {
  pinMode(LED_BUILTIN, OUTPUT);

  Serial.println("Configuring access point...");
  WiFi.softAP(ssid, password); // You can remove the password parameter if you want the AP to be open.
  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
  server.begin();
  Serial.println("Server started");
}

void wifi_AP() {  // ajouter menu vitesse et fréquence d'écriture
  while (true) {
  WiFiClient client = server.available();   // listen for incoming clients

  if (client) {                             // if you get a client,
    //Serial.println("New Client.");          // print a message out the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected()) {            // loop while the client's connected
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        if (c == '\n') {                    // if the byte is a newline character

          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println();

            // the content of the HTTP response follows the header:
            client.print("Click <a href=\"/H\">here</a> to turn ON the LED.<br>");
            client.print("Click <a href=\"/L\">here</a> to turn OFF the LED.<br>");

            // The HTTP response ends with another blank line:
            client.println();
            // break out of the while loop:
            break;
          } else {    // if you got a newline, then clear currentLine:
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }

        // Check to see if the client request was "GET /H" or "GET /L":
        if (currentLine.endsWith("GET /H")) {
          digitalWrite(LED_BUILTIN, HIGH);               // GET /H turns the LED on
        }
        if (currentLine.endsWith("GET /L")) {
          digitalWrite(LED_BUILTIN, LOW);                // GET /L turns the LED off
        }
      }
    }
    // close the connection:
    client.stop();
    Serial.println("Client Disconnected.");
  }
  }
  ESP.restart();
}

///////////////
/* PAGE HTML */
///////////////
// Peut-être externaliser la page html dans un fichier txt
String html ="<!DOCTYPE html> \
<html> \
<body> \
<center><h1>ESP32 IFV Soft access point</h1></center> \
<center><h2>Web Server</h2></center> \
</body> \
</html>";

/////FONCTIONS COMMUNES//////

void vigie_Wifi() {
  etat_bouton_wifi = digitalRead(bouton_wifi);
  if ( etat_bouton_wifi == HIGH && sem_wifi == false ) {
    sem_wifi = true;
    wifi_minuteur = millis();
  }
  if ( etat_bouton_wifi == HIGH && sem_wifi == true && (millis() - wifi_minuteur) >= tempo_bouton_wifi ) {
    Serial.println("Mode wifi");
    stop_moteur();
    stop_compteur();
    start_wifiAP();
    wifi_AP();
  }
} 

void setup() {
  Serial.begin(9600); // serial just for feedback
  delay(3000);
  pinMode(LED,OUTPUT); // LED bouton wifi et erreurs
  pinMode(bouton_wifi, INPUT); // bouton wifi
  //Serial.println("balise 0");
  rtc_init();         // RTC
  fichier[0] = '/';
  delay(3000);
  sd_init ();         // initialisation de la carte SD
  delay(3000);
  sem_wifi = false;   // initialisation du sémaphore
  /* compteur de pulsations */
  sem_scribe = false; // initialisation du sémaphore
  //pinMode(pulsePin,INPUT_PULLUP);
  start_compteur();
  delay(5000);
  start_moteur();     // moteur
}

void loop (){
  vigie_Wifi();
  compte_tour();
  scribe_sd();
}
